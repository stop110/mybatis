package com.yuan.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.yuan.bean.User;
import com.yuan.dao.UserDao;

public class Mybatis2 {

	public static void main(String[] args) throws IOException {
		String resource = "SqlMapConfig.xml";
		InputStream reader = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		// true 标识自动提交，否则需要使用commit方法才会提交。默认就是false
		SqlSession session = sqlMapper.openSession();

		try {
			User user = new User();
			user.setId(8);
			user.setUsername("黄黄");
			user.setPassword("666666");
			user.setType("boss");
			UserDao dao = session.getMapper(UserDao.class);
//			dao.insertUser(user);
//			dao.updateUser(user);
//			dao.deleteUser(8);
//			List<User> list=dao.queryList("z", "admin");
			Map map=new HashMap();
			map.put("username","z");
			map.put("type","admin");
			List<User> list=dao.queryList1(map);
			System.out.println(list.size());
			session.commit();
		} finally {
			session.close();
		}

	}
}
