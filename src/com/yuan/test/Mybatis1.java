package com.yuan.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import com.yuan.bean.User;
import com.yuan.dao.UserDao;

public class Mybatis1 {
	@Test
	public void wan() throws IOException {
		String resource = "SqlMapConfig.xml";
		InputStream reader = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);
		SqlSession session=sqlMapper.openSession(true);	
		
		UserDao uDao=session.getMapper(UserDao.class);
//		System.out.println(uDao);
		User us=uDao.selectOne(5);
		System.out.println(us);
		
		List<User> xx=uDao.selectAll();
		System.out.println(xx.size());
	}
}
