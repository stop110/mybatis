package com.yuan.bean.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import com.yuan.bean.UserMapper;

public class MyBatisTest {
	@Test
	public void wan(){
		try {
			String resource = "SqlMapConfig.xml";
			InputStream reader = Resources.getResourceAsStream(resource);
			SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);
			SqlSession sqlSession=sqlMapper.openSession();
			UserMapper xm=sqlSession.selectOne("com.yuan.bean.UserMapper.selectUser", "2");
			System.out.println(xm.getUsername());
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
