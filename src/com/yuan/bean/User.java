package com.yuan.bean;
/**
 * 
*    
* 项目名称：mybatis   
* 类名称：User   
* 类描述：   
* 创建人：Administrator   
* 创建时间：2018年10月10日 下午2:59:48   
* 修改人：Administrator   
* 修改时间：2018年10月10日 下午2:59:48   
* 修改备注：   
* @version  1  
*
 */
public class User {
	//管理员表
	private int id;
	private String username;
	private String password;
	private String type;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "User [" +this.id+","+ this.username+","+this.password+","+this.type+ "]";
	}
	
	
}
