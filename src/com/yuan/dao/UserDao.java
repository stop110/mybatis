package com.yuan.dao;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.yuan.bean.User;

public interface UserDao {
	/**
	 * 1.单个参数的mybatis不会走特殊处理
	 * 		{@link #toString()}
	 * 2.传入对象POJO
	 * 		#{对象的属性名称}
	 * 3.多个参数。mybatis做特殊处理。会把传入的参数自动封装成Map类型
	 * 		map的key值就是从param1.....paramN....
	 * 		map.put("param1",username);
	 * 		map.put("param2",type);
	 * 		@param("username")可以使用这个注解 来自定义map封装的key值...
	 *	4.直接传Map
	 */
	//查询所有
	public List<User> selectAll();
	//查询指定
	public User selectOne(int id);
	//新增
	public void insertUser(User user);
	//修改
	public void updateUser(User user);
	//删除
	public void deleteUser(int id);
	//多条件查询
	public List<User> queryList(@Param("username")String username,@Param("type")String type);
	//
	public List<User> queryList1(Map map);	
	//测试 
}
